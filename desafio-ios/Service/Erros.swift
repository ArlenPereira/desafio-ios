//
//  Erros.swift
//  desafio-ios
//
//  Created by Arlen Ricardo Pereira on 05/11/17.
//  Copyright © 2017 Arlen Ricardo Pereira. All rights reserved.
//

import UIKit

struct ConcreteErrors {
    
    static let error1 = "Only the first 1000 search results are available"
    static let error2 = "Validation Failed"
    static let error1003 = "Site não encontrado"
    static let error1009 = "Sem sinal de Internet"
    static let errorLimits = "Apenas 1000 registros estão disponiveis"
    static let errorValidation = "Falha de validação"
    static let errorUnknown = "Erro desconhecido"
    static let concreteErrorDomain = "com.arlen.concrete"
    static let pullRequestEmpty = "Nenhum pull Request encontrado"
    
    static let genericErrorCode = -1
    
    static func afMessage(code: Int) -> String {
        switch code {
        case -1003:
            return ConcreteErrors.error1003
            break
        case -1009:
            return ConcreteErrors.error1009
            break
        default: break
        }
        return ConcreteErrors.errorUnknown
    }
    
    static func ghMessage(message: String) -> String {
        
        switch message {
        case ConcreteErrors.error1 :
            return ConcreteErrors.errorLimits
        case ConcreteErrors.error2 :
            return ConcreteErrors.errorValidation
        default: break
        }
        return ConcreteErrors.errorUnknown
    }
    
    static func afError(code: Int) -> Error {
        
        let message = ConcreteErrors.afMessage(code: code)
        
        let error = NSError(domain: ConcreteErrors.concreteErrorDomain,
                            code: code,
                            userInfo: [kCFErrorLocalizedDescriptionKey as AnyHashable as! String: message])
        return error
    }
    
    static func ghError(message: String) -> Error {
        
        let message = ConcreteErrors.ghMessage(message: message)
        
        let error = NSError(domain: ConcreteErrors.concreteErrorDomain,
                            code: ConcreteErrors.genericErrorCode,
                            userInfo: [kCFErrorLocalizedDescriptionKey as AnyHashable as! String: message])
        return error
    }
    
    static var emptyPullRequestsError: Error {
        
        let error = NSError(domain: ConcreteErrors.concreteErrorDomain,
                            code: ConcreteErrors.genericErrorCode,
                            userInfo: [kCFErrorLocalizedDescriptionKey as AnyHashable as! String: ConcreteErrors.pullRequestEmpty])
        return error
    }
}
