//
//  Network.swift
//  desafio-ios
//
//  Created by Arlen Ricardo Pereira on 03/11/17.
//  Copyright © 2017 Arlen Ricardo Pereira. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

protocol NetworkDelegate {
    func didUpdateRepositories()
    func didUpdatePullRequests(pullRequests: [PullRequest])
    func didFail(error: Error)
}

class Network: NSObject {

    let authentication = "31860f0f31007d376bf59f3b3ba138165c58c50d"
    let addressURL = "https://api.github.com/"
    let titleSearch = "language:Java"
    let sortSearch = "stars"
    var pageSearch = 1
    let errorMessage = "message"
    let errorUserDomain = "Arlen"
    let subJsonRepository = "items"
 
    static let shared = Network()
    var repositoriesArray = [Repository]()
    var delegate: NetworkDelegate?
    var isFetching = false
    var isError = false
    
    func fetchRepositories(page: Int, handler: @escaping (Error?) -> ()) {
        let repositoryHttp = "\(addressURL)search/repositories?access_token=\(authentication)&q=\(titleSearch)&sort=\(sortSearch)&page=\(page)"
        
        isFetching = true
        
        Alamofire.request(repositoryHttp).responseJSON { (responseData) -> Void in
            self.isFetching = false
            guard responseData.error == nil else {
                self.isError = true
                let error = responseData.error! as NSError
                let concreteError = ConcreteErrors.afError(code: error.code)
                self.delegate?.didFail(error: concreteError)
                return
            }
            
            if ((responseData.result.value) != nil) {
                let swiftyJsonVar = JSON(responseData.result.value!)
                guard !swiftyJsonVar[self.errorMessage].exists() else {
                    self.isError = true
                    let message = swiftyJsonVar[self.errorMessage].stringValue
                    let concreteError = ConcreteErrors.ghError(message: message)
                    self.delegate?.didFail(error: concreteError)
                    return
                }
                
                var repositories = [Repository]()
                for (_, item):(String, JSON) in swiftyJsonVar[self.subJsonRepository] {
                    let repository = Repository(json: item)
                    repositories.append(repository)
                }
                
                if page == 1 {
                    self.repositoriesArray = repositories
                } else {
                    self.repositoriesArray.append(contentsOf: repositories)
                }
                
                handler(nil)
                self.delegate?.didUpdateRepositories()
            }
        }
    }
    
    func fetchNextPage() {
    
        fetchRepositories(page: pageSearch) { (error) in
            guard error == nil else { return }
            
            self.pageSearch += 1
        }
    }
    
    func fetchPullRequests(owner: String, repository: String, handler: @escaping (Error?, [PullRequest]?) -> ()) {
        let pullRequestHttp = "\(addressURL)repos/\(owner)/\(repository)/pulls"
        
        Alamofire.request(pullRequestHttp).responseJSON { (responseData) -> Void in
            guard responseData.error == nil else {
                self.isError = true
                let error = responseData.error! as NSError
                let concreteError = ConcreteErrors.afError(code: error.code)
                handler(concreteError, nil)
                return
            }
            
            if ((responseData.result.value) != nil) {
                let swiftyJsonVar = JSON(responseData.result.value!)
                guard !swiftyJsonVar[self.errorMessage].exists() else {
                    self.isError = true
                    let message = swiftyJsonVar[self.errorMessage].stringValue
                    let concreteError = ConcreteErrors.ghError(message: message)
                    handler(concreteError, nil)
                    return
                }
                
                var pullRequests = [PullRequest]()
                for (_, item):(String, JSON) in swiftyJsonVar {
                    let pullRequest = PullRequest(json: item)
                    pullRequests.append(pullRequest)
                }
                
                handler(nil, pullRequests)
            }
        }
    }
}
