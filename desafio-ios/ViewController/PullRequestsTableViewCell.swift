//
//  PullRequestsTableViewCell.swift
//  desafio-ios
//
//  Created by Arlen Ricardo Pereira on 03/11/17.
//  Copyright © 2017 Arlen Ricardo Pereira. All rights reserved.
//

import UIKit
import SDWebImage

class PullRequestsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var prTitleLabel: UILabel!
    @IBOutlet weak var prBodyLabel: UILabel!
    @IBOutlet weak var prUsernameLabel: UILabel!
    @IBOutlet weak var prFullNameLabel: UILabel!
    @IBOutlet weak var prUserImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        prUserImageView.layer.cornerRadius = prUserImageView.frame.size.width/2
        prUserImageView.clipsToBounds = true
    }
    
    var pullRequestCell: PullRequest? {
        didSet {
            if let title = pullRequestCell?.title { prTitleLabel.text = title }
            if let body = pullRequestCell?.body { prBodyLabel.text = body }
            if let username = pullRequestCell?.user.authorPR { prUsernameLabel.text = username }
            if let type = pullRequestCell?.user.typePR { prFullNameLabel.text = type }
            if let photo = pullRequestCell?.user.photoAuthorPRUrl {
                prUserImageView.sd_setShowActivityIndicatorView(true)
                prUserImageView.sd_setIndicatorStyle(.white)
                prUserImageView.sd_setImage(with: NSURL(string: photo) as URL!, placeholderImage: nil, options: [.continueInBackground])
            }
        }
    }

//    override func setSelected(_ selected: Bool, animated: Bool) {
//        super.setSelected(selected, animated: animated)
//
//        // Configure the view for the selected state
//    }
}
