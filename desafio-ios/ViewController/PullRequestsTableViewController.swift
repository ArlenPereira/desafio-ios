//
//  PullRequestsTableViewController.swift
//  desafio-ios
//
//  Created by Arlen Ricardo Pereira on 03/11/17.
//  Copyright © 2017 Arlen Ricardo Pereira. All rights reserved.
//

import UIKit
import EMAlertController
import ANLoader

class PullRequestsTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var pullRequestSegue: Repository?
    
    var cellId = "pullRequestCell"
    var segueId = "pullRequestDetailSegue"
    let nibNamePullRequest = "PullRequestsTableViewCell"
    let sectionNumber = 1
    var network: Network{ return Network.shared }
    let refreshControl = UIRefreshControl()
    var pullRequests: [PullRequest]?
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var openedLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = pullRequestSegue!.title
        tableView.register(UINib(nibName: nibNamePullRequest, bundle: nil), forCellReuseIdentifier: cellId)
        refreshControl.addTarget(self, action: #selector(PullRequestsTableViewController.refreshing), for: UIControlEvents.valueChanged)
        
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
        ANLoader.activityBackgroundColor = .darkGray
        ANLoader.showLoading("", disableUI: true)
        
        let owner = pullRequestSegue!.owner.author
        let repository = pullRequestSegue!.title
        
        network.fetchPullRequests(owner: owner, repository: repository) { (error, pullRequests) in
            ANLoader.hide()
            guard error == nil else {
                self.openedLabel.isHidden = true
                let alert = EMAlertController(icon: #imageLiteral(resourceName: "github_icon"), title: "Alerta", message: error!.localizedDescription)
                let action1 = EMAlertAction(title: "CANCEL", style: .cancel) { }
                alert.addAction(action: action1)
                self.present(alert, animated: true, completion: nil)
                return
            }
            
            let pullRequestCount = pullRequests?.count ?? 0
            guard pullRequestCount > 0 else {
                self.openedLabel.isHidden = true
                let pullRequestEmptyError = ConcreteErrors.emptyPullRequestsError
                let alert = EMAlertController(icon: #imageLiteral(resourceName: "github_icon"), title: "Alerta", message: pullRequestEmptyError.localizedDescription)
                let action1 = EMAlertAction(title: "CANCEL", style: .cancel) { }
                alert.addAction(action: action1)
                self.present(alert, animated: true, completion: nil)
                return
            }
            self.pullRequests = pullRequests
            
            self.openedLabel.text = "\(String(pullRequests?.count ?? 0)) Open"
            self.tableView.reloadData()
        }
    }
    
    // MARK: - Table view data source

    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return sectionNumber
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        
        if pullRequests?.count ?? 0 > 0 {
            ANLoader.hide()
        }
        return pullRequests?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! PullRequestsTableViewCell
        cell.pullRequestCell = pullRequests?[indexPath.item]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: self.segueId, sender: indexPath)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == self.segueId {
            let destination = segue.destination as? PullRequestDetailViewController
            let selectedIndexPath = sender as! NSIndexPath
            let index = pullRequests?[selectedIndexPath.item]
            destination?.pullRequestDetailSegue = index
        }
    }
    
    @objc func refreshing() {
        network.fetchNextPage()
        refreshControl.endRefreshing()
    }
}
