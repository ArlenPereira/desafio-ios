//
//  RepositoriesTableViewController.swift
//  desafio-ios
//
//  Created by Arlen Ricardo Pereira on 03/11/17.
//  Copyright © 2017 Arlen Ricardo Pereira. All rights reserved.
//

import UIKit
import ANLoader
import EMAlertController

class RepositoriesTableViewController: UITableViewController {

    let cellId = "repositoryCell"
    let segueId = "pullRequestSegue"
    let nibNameRepo = "RepositoriesTableViewCell"
    let sectionNumber = 1
    var network: Network { return Network.shared }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        network.delegate = self
        
        tableView.register(UINib(nibName: nibNameRepo, bundle: nil), forCellReuseIdentifier: cellId)
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(RepositoriesTableViewController.refreshing), for: UIControlEvents.valueChanged)
        
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
        ANLoader.activityBackgroundColor = .darkGray
        ANLoader.showLoading("", disableUI: true)
        if network.repositoriesArray.isEmpty { network.fetchNextPage() }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return sectionNumber
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if network.repositoriesArray.count > 0 {
            ANLoader.hide()
        }
        return network.repositoriesArray.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! RepositoriesTableViewCell
        cell.repositoriesCell = network.repositoriesArray[indexPath.item]
        return cell
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let count = self.network.repositoriesArray.count
        if indexPath.row > count - 10 {
            self.network.fetchNextPage()
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: self.segueId, sender: indexPath)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == self.segueId {
            let destination = segue.destination as? PullRequestsTableViewController
            let selectedIndexPath = sender as! NSIndexPath
            let index = network.repositoriesArray[selectedIndexPath.item]
            destination?.pullRequestSegue = index
        }
    }
    
    @objc func refreshing() {
        network.fetchNextPage()
        refreshControl?.endRefreshing()
    }
}

extension RepositoriesTableViewController: NetworkDelegate {
    func didUpdatePullRequests(pullRequests: [PullRequest]) { }
    
    func didFail(error: Error) {
        ANLoader.hide()
        let alert = EMAlertController(icon: UIImage(named: "github_icon"), title: "Alerta", message: error.localizedDescription)
        let action1 = EMAlertAction(title: "CANCEL", style: .cancel)
        alert.addAction(action: action1)
        present(alert, animated: true, completion: nil)
    }
    
    func didUpdateRepositories() { tableView.reloadData() }
}
