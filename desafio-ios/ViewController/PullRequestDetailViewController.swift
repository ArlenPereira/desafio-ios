//
//  PullRequestDetailViewController.swift
//  desafio-ios
//
//  Created by Arlen Ricardo Pereira on 04/11/17.
//  Copyright © 2017 Arlen Ricardo Pereira. All rights reserved.
//

import UIKit
import WebKit
import ANLoader
import EMAlertController

class PullRequestDetailViewController: UIViewController, WKNavigationDelegate {

    var pullRequestDetailSegue: PullRequest?
    var network: Network{ return Network.shared }
    
    @IBOutlet weak var webView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = String(describing: pullRequestDetailSegue!.title)
        ANLoader.activityBackgroundColor = .darkGray
        ANLoader.showLoading("", disableUI: true)
        let addressURL = pullRequestDetailSegue!.html_url
        let url = URL(string: addressURL)
        let request = URLRequest(url: url!)
        webView.navigationDelegate = self
        webView.load(request)
    }
    
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        ANLoader.hide()
    }

    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        ANLoader.hide()
        print(error.localizedDescription)
        let alert = EMAlertController(icon: #imageLiteral(resourceName: "github_icon"), title: "Alerta", message: error.localizedDescription)
        let action1 = EMAlertAction(title: "CANCEL", style: .cancel) { }
        alert.addAction(action: action1)
        self.present(alert, animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
