//
//  RepositoriesTableViewCell.swift
//  desafio-ios
//
//  Created by Arlen Ricardo Pereira on 03/11/17.
//  Copyright © 2017 Arlen Ricardo Pereira. All rights reserved.
//

import UIKit
import SDWebImage

class RepositoriesTableViewCell: UITableViewCell {
    
    @IBOutlet weak var repositoryLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var forksLabel: UILabel!
    @IBOutlet weak var starsLabel: UILabel!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var nameLastNameLabel: UILabel!
    @IBOutlet weak var userImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        userImageView.layer.cornerRadius = userImageView.frame.size.width/2
        userImageView.clipsToBounds = true
    }
    
    var repositoriesCell: Repository? {
        didSet {
            if let title = repositoriesCell?.title { repositoryLabel.text = title }
            if let body = repositoriesCell?.body { descriptionLabel.text = body }
            if let stars = repositoriesCell?.starNumber { starsLabel.text = String(stars) }
            if let forks = repositoriesCell?.forkNumber { forksLabel.text = String(forks) }
            if let username = repositoriesCell?.owner.author { usernameLabel.text = username }
            if let type = repositoriesCell?.owner.type { nameLastNameLabel.text = type }
            if let photo = repositoriesCell?.owner.photoAuthorUrl {
                userImageView.sd_setShowActivityIndicatorView(true)
                userImageView.sd_setIndicatorStyle(.white)
                userImageView.sd_setImage(with: NSURL(string: photo) as URL!, placeholderImage: nil, options: [.continueInBackground])
            }
        }
    }

//    override func setSelected(_ selected: Bool, animated: Bool) {
//        super.setSelected(selected, animated: animated)
//
//        // Configure the view for the selected state
//    }
}
