//
//  PullRequest.swift
//  desafio-ios
//
//  Created by Arlen Ricardo Pereira on 04/11/17.
//  Copyright © 2017 Arlen Ricardo Pereira. All rights reserved.
//

import UIKit
import SwiftyJSON

class PullRequest: NSObject {
    
    var idPR: Int = 0
    var title: String = ""
    var body: String = ""
    var html_url: String = ""
    var user: PullRequestUser
    
    init(json: JSON) {
        idPR = json["id"].intValue
        title = json["title"].stringValue
        body = json["body"].stringValue
        html_url = json["html_url"].stringValue
        user = PullRequestUser(json: json["user"])
    }
}

class PullRequestUser: NSObject {
    
    var idPRUser: Int = 0
    var authorPR: String = ""
    var photoAuthorPRUrl: String = ""
    var typePR: String = ""
    
    init(json: JSON) {
        idPRUser = json["id"].intValue
        authorPR = json["login"].stringValue
        photoAuthorPRUrl = json["avatar_url"].stringValue
        typePR = json["type"].stringValue
    }
}
