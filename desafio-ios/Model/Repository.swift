//
//  Repositories.swift
//  desafio-ios
//
//  Created by Arlen Ricardo Pereira on 03/11/17.
//  Copyright © 2017 Arlen Ricardo Pereira. All rights reserved.
//

import UIKit
import SwiftyJSON

class Repository: NSObject {
    
    var idRepo: Int = 0
    var title: String = ""
    var body: String = ""
    var starNumber: Int = 0
    var forkNumber: Int = 0
    var owner: RepositoryOwner
    
    init(json: JSON) {
        idRepo = json["id"].intValue
        title = json["name"].stringValue
        body = json["description"].stringValue
        starNumber = json["watchers"].intValue
        forkNumber = json["forks"].intValue
        owner = RepositoryOwner(json: json["owner"])
    }
}

class RepositoryOwner: NSObject {
    
    var idOwner: Int = 0
    var author: String = ""
    var photoAuthorUrl: String = ""
    var type: String = ""

    init(json: JSON) {
        idOwner = json["id"].intValue
        author = json["login"].stringValue
        photoAuthorUrl = json["avatar_url"].stringValue
        type = json["type"].stringValue
    }
}

